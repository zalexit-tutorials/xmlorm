# Демо проект для конвертера XML-dataclass/ORM model

## Как это работает
XmlParser().parse_file(file_path, serializer_class)

принимает путь к файлу и класс сериализатора (XmlSerializer)
и возвращает объект, содержание которого составлено из контента 
xml документа

XmlSerializer должен содержать Meta.model и Meta.fields,
которые описывают связь Xml полей и моделей.

Примеры использования - в тестах.