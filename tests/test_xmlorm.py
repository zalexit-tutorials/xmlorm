from dataclasses import dataclass

from xmlorm.xml_field import StringXmlField
from xmlorm.xml_parser import XmlParser
from xmlorm.xml_serializer import XmlSerializer


@dataclass
class Header(object):
    format = StringXmlField()


@dataclass
class Message(object):
    header: Header = None


class HeaderSerializer(XmlSerializer):
    format = StringXmlField()

    class Meta(object):
        model = Header
        fields = {'format': 'Format'}


class MessageSerializer(XmlSerializer):
    header = HeaderSerializer

    class Meta(object):
        model = Message
        fields = {'header': 'Header'}


def test_parsing():
    message = XmlParser().parse_file(
        './input/some.xml',
        MessageSerializer,
        encoding='utf8',
    )
    target_text = 'http://v8.1c.ru/edi/edi_stnd/EnterpriseData/1.10'

    assert message.header.format.text == target_text
