from dataclasses import dataclass
from typing import Any

from lxml import etree


@dataclass
class XmlField:
    to_field: str = 'to_field'
    xml_tag: str = 'xml_tag'
    many: bool = False

    def to_element(self, orm_object: Any) -> etree.Element:
        pass

    def from_element(self, element):
        pass


class StringXmlField(XmlField):
    text: str

    def from_element(self, element):
        self.text = element.text

    def to_element(self, orm_object) -> etree.Element:
        field_value = getattr(orm_object, self.to_field)
        new_element = etree.Element(self.xml_tag)
        new_element.text = field_value
        return new_element


class FixedXmlField(XmlField):
    """
    Поле, значение которого всегда одинаково.

    Формируется элемент <xml_tag>fixed_value</xml_tag>
    """
    def __init__(self, xml_tag='xml_tag', fixed_value=''):
        self.fixed_value = fixed_value
        self.xml_tag = xml_tag

    def to_element(self, orm_object: Any) -> etree.Element:
        new_element = etree.Element(self.xml_tag)
        new_element.text = self.fixed_value
        return new_element


class ReferenceXmlField(XmlField):
    """Поле для связи с внешними системами (1С)."""
    def __init__(self, xml_tag='Ссылка', get_uuid=None):
        self.get_uuid = get_uuid
        self.xml_tag = xml_tag

    def to_element(self, orm_object: Any) -> etree.Element:
        uuid = self.get_uuid(orm_object)
        new_element = etree.Element(self.xml_tag)
        new_element.text = str(uuid)
        return new_element
