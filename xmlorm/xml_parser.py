import datetime
from typing import Tuple

from lxml import etree

from xmlorm.xml_serializer import XmlSerializer


class XmlParser(object):

    def parse_file(
        self,
        file_path: str,
        serializer_class: type(XmlSerializer),
        *args,
        **kwargs,
    ):
        with open(file_path, 'rt', **kwargs) as xml_file:
            tree = etree.parse(xml_file)
            root = tree.getroot()

            serializer = serializer_class()
            parsed_object = serializer.from_xml(root)
            return parsed_object

    def generate_1c_document(self, orm_object) -> Tuple[etree.Element, etree.Element]:
        msg_url = 'http://www.1c.ru/SSL/Exchange/Message'
        ns_map = {'msg': msg_url}
        message = etree.Element('Message', nsmap=ns_map)
        header = etree.SubElement(message, f'{{{msg_url}}}Header')
        etree.SubElement(header, f'{{{msg_url}}}Format').text = (
            'http://v8.1c.ru/edi/edi_stnd/EnterpriseData/1.10'
        )
        etree.SubElement(
            header, f'{{{msg_url}}}CreationDate').text = (
            datetime.datetime.now().isoformat()
        )
        for some_version in (
        '1.3', '1.4', '1.5', '1.6', '1.7', '1.8', '1.10'):
            etree.SubElement(
                header, f'{{{msg_url}}}AvailableVersion',
            ).text = some_version
        body_element = etree.SubElement(
            message,
            'Body',
            xmlns='http://v8.1c.ru/edi/edi_stnd/EnterpriseData/1.10',
        )
        return message, body_element

    def generate_xml(self, orm_object, serializer_class) -> str:
        message, body = self.generate_1c_document(orm_object)

        serializer = serializer_class()
        xml_element = serializer.to_xml(orm_object)
        body.append(xml_element)
        tree = etree.ElementTree(message)

        xml_bytes = etree.tostring(tree, pretty_print=True, encoding='utf8')
        title = '<?xml version="1.0" encoding="utf-8"?>\n'
        return title + xml_bytes.decode('utf8')
