import datetime
import inspect
from collections import defaultdict
from typing import List, Dict, Any, Tuple

from loguru import logger as log
from lxml import etree

from xmlorm.xml_field import XmlField


def build_property_from_class(self, xml_field, serializer_class):
    """Аналог fk. Строим объект от поля."""


def build_property(field, element_list):

    if inspect.isclass(field):
        if issubclass(field, XmlSerializer):
            serializer = field()
            # сейчас считается, что поле Serializer many=False всегда
            element = element_list[0]
            new_property = serializer.from_xml(element)
            return new_property
    else:
        if isinstance(field, XmlField):
            if field.many is False:
                element = element_list[0]
                field.from_element(element)
                return field

    return None


class XmlSerializer:
    """Many заставит итерироваться по всем элементам указанного свойства.

    Если указан call_me=True, то перед итерированием свойство будет вызвано.
    .property()
    """
    def __init__(self, to_field='', many=False, call_me=False):
        self.to_field = to_field
        self.many = many
        self.call_me = call_me

    def from_xml(self, root_element):
        if self.Meta.model is None:
            raise ValueError('Meta.model not specified.')

        child_map = defaultdict(list)

        for child in root_element:
            short_tag = child.tag[child.tag.index('}') + 1:]
            child_map[short_tag].append(child)
        child_dict = dict(child_map)
        new_object = self.Meta.model()

        for field_name, source_name in self.Meta.fields.items():
            field = getattr(self, field_name)
            log.debug(f'building {field_name} from {source_name}')
            new_property = build_property(field, child_dict[source_name])
            setattr(new_object, field_name, new_property)
            log.debug('sssssssssssss')
        return new_object

    def _generate_message(self) -> Tuple[etree.Element, etree.Element]:
        msg_url = 'http://www.1c.ru/SSL/Exchange/Message'
        ns_map = {'msg': msg_url}
        message = etree.Element('Message', nsmap=ns_map)
        header = etree.SubElement(message, f'{{{msg_url}}}Header')
        etree.SubElement(header, f'{{{msg_url}}}Format').text = (
            'http://v8.1c.ru/edi/edi_stnd/EnterpriseData/1.10'
        )
        etree.SubElement(
            header, f'{{{msg_url}}}CreationDate').text = (
            datetime.datetime.now().isoformat()
        )
        for some_version in ('1.3', '1.4', '1.5', '1.6', '1.7', '1.8', '1.10'):
            etree.SubElement(
                header, f'{{{msg_url}}}AvailableVersion',
            ).text = some_version
        body_element = etree.SubElement(
            message,
            'Body',
            xmlns='http://v8.1c.ru/edi/edi_stnd/EnterpriseData/1.10',
        )
        return message, body_element

    def generate_1c_message(self, orm_object: Any) -> etree.ElementTree:
        """
        Формирование XML документа для 1С.

        обязательные поля:
        Header = (Format, CreationDate, AvailableVersion(n штук);
        Body, в который пихаем весь контент.
        """
        message, body = self._generate_message()
        etree.SubElement(body, self.to_xml(orm_object))
        msg_bytes = etree.tostring(message)
        return msg_bytes.decode('utf8')

    def to_xml(self, orm_object: Any) -> etree.Element:
        log.debug('to_xml')
        new_element = etree.Element(self.Meta.xml_tag)
        for field_name in self.Meta.fields:
            field = getattr(self, field_name)
            if field.to_field == 'sale.billing_from.counterparty':
                x = 1
            if inspect.isclass(field):
                # класс - это ссылка на другой сериализатор
                raise ValueError('must be serializer instance, not class')

            if isinstance(field, XmlSerializer):
                # разбор объекта целиком
                next_object = orm_object
                for field_part in field.to_field.split('.'):
                    next_object = getattr(next_object, field_part)
                if field.call_me:
                    next_object = next_object()
                if field.many:
                    next_elements = [
                        field.to_xml(next_one)
                        for next_one in next_object
                    ]
                else:
                    next_elements = [field.to_xml(next_object)]
                new_element.extend(next_elements)
            else:
                # поле - экземпляр XmlField)
                field_element = field.to_element(orm_object)
                new_element.append(field_element)
        return new_element

    class Meta(object):
        xml_tag = 'undefined'
        model = None
        fields: Dict[str, str] = {}


